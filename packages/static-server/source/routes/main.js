import React from 'react'
import { match } from 'react-router'
import config from 'config'

import { createStore, applyMiddleware } from 'redux'
import { createMemoryHistory } from 'react-router'
import createSagaMiddleware from 'redux-saga'
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux'

import rootReducer from 'core/lib/app/reducer'
import configureRouter from 'core/lib/app/router'

import renderLayout from '@/render/layout'
import render from '@/render'

const title = config.get('name')

import rootSaga from '@/rootSaga'

const routes = configureRouter()()

export default (req, res) => {
    const memoryHistory = createMemoryHistory(req.url)
    const sagaMiddleware = createSagaMiddleware()
    const routerMW = routerMiddleware(memoryHistory)
    const store = createStore(
        rootReducer,
        applyMiddleware(sagaMiddleware, routerMW)
    )
    const history = syncHistoryWithStore(memoryHistory, store)

    match(
        {
            routes,
            location: req.url,
            history
        },
        (error, redirectLocation, renderProps) => {
            if (error) {
                res.status(500).send(error.message)
            } else if (redirectLocation) {
                res.redirect(
                    302,
                    redirectLocation.pathname + redirectLocation.search,
                )
            } else if (renderProps) {
                sagaMiddleware.run(rootSaga).done.then(() => {
                    const rootMarkup = render(React)(renderProps, store)
                    const initialState = store.getState()

                    res.status(200).send(
                        renderLayout({
                            title,
                            rootMarkup,
                            initialState,
                        }),
                    )
                })
            } else {
                res.status(404).send('Not found')
            }
        },
    )
}
