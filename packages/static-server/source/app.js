import express from 'express'
import config from 'config'
import proxy from 'http-proxy-middleware'

import mainRoute from '@/routes/main'

const makeApp = (app = express()) => {
    const apiPort = config.get('api.port')
    const apiHost = config.get('api.host')

    app.use(
        '/api',
        proxy({
            target: `http://${apiHost}:${apiPort}`,
            pathRewrite: { '^/api': '' },
        }),
    )

    const staticDir = config.get('folders.client.static')
    console.info('assets folder:', staticDir)
    app.use('/static', express.static(staticDir))

    app.use('/', mainRoute)

    return app
}

export default makeApp
