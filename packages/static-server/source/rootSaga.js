import { call, select, put, fork, race, cancel, take } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import config from 'config'

import { reqTweets, TWEETS_RESPONSE } from 'core/lib/tweets/reducer'
import { getPathname } from 'core/lib/app/selectors'

import { configResponse } from 'core/lib/app/reducer/app'

import fetchWatchers from 'core/lib/app/sagas'

const re = /\/tweets\/(\d+)/

const watchFetchTweet = function*() {
    const pathname = yield select(getPathname)

    if (re.test(pathname)) {

        const id = re.exec(pathname)[1]
        yield put(reqTweets({ id }))
        yield take(TWEETS_RESPONSE)
    }
}


export default function*() {
    yield put(configResponse({ config: config.get('staticServer') }))

    const fetchWorkersTask = yield fork(fetchWatchers)

    const { tiemout } = yield race({
        workers: call(watchFetchTweet),
        tiemout: call(delay, 2000)
    })

    yield cancel(fetchWorkersTask)

    if (tiemout) {
        console.log('timed out')
    }

    yield put(configResponse({ config: config.get('client') }))
}
