var WAIT = 5000

module.exports = {
    EncodeURL(browser) {
        browser
            .url(`${browser.launchUrl}/`)
            .waitForElementVisible('.searchPage', WAIT)
            .setValue('.searchPage input', '@nodejs')
            .waitForElementVisible('.searchPage .tweetCard:first-child', WAIT)
            .assert.elementPresent('.searchPage .tweetCard:first-child')
            .end()
    },
}
