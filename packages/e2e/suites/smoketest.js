var WAIT = 5000

module.exports = {
    Smoketest(browser) {
        browser
            .url(`${browser.launchUrl}/`)
            .waitForElementVisible('.searchPage', WAIT)
            .assert.containsText('.searchPage', 'Search Tweet')
            .end()
    },
}
