var WAIT = 5000

module.exports = {
    SearchTweet(browser) {
        browser
            .url(`${browser.launchUrl}/`)
            .waitForElementVisible('.searchPage', WAIT)
            .setValue('.searchPage input', 'nodejs')
            .waitForElementVisible('.searchPage .tweetCard:first-child', WAIT)
            .assert.elementPresent('.searchPage .tweetCard:first-child')
            .click('.searchPage .tweetCard:first-child .cardDetails a')
            .waitForElementVisible('.details', WAIT)
            .assert.elementPresent('.details')
            .end()
    },
}
