import styled from 'styled-components'

const Viewport = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: stretch;
    position: relative;
    height: max-content;
    min-height: 100vh;
`

export default Viewport
