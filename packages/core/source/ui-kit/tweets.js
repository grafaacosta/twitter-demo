import styled from 'styled-components'

export const ProfilePicContainer = styled.div`
    width: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const DescriptionContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
    padding: 10px;
`

export const UserName = styled.p`
    color: ${props => (props.large ? 'white' : 'rgba(0,0,0, 0.54)')};
    font-size: ${props => (props.large ? '1.2em' : '0.875rem')};
    font-weight: 400;
    font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
    line-height: 1.46429em;
    margin: 0px;
    display: flex;
    align-items: center;
`

export const TweetText = styled.h2`
    flex: 1;
    font-size: 1.3em;
    font-weight: 400;
    font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
    margin: 0px;
    margin-top: 5px;
    font-size: 1.2em;
`

export const CardActions = styled.div`
    display: flex;
    justify-content: flex-end;
`
