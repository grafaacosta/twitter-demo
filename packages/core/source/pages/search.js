import React from 'react'
import styled from 'styled-components'

import Paper from 'material-ui/Paper'

import Tweets from '@/tweets/view/list'
import SearchTweet from '@/tweets/view/search'

const PaperContainer = styled(Paper)`
    display: flex;
    flex: 1;
    flex-direction: column;
    padding: 10px;
    margin: 10px;
`

const SearchPage = () => (
    <PaperContainer className="searchPage">
        <SearchTweet />
        <Tweets />
    </PaperContainer>
)

export default SearchPage
