import React from 'react'
import Viewport from '@/ui-kit/layout/viewport'

const IndexPage = ({ children }) => <Viewport>{children}</Viewport>

export default IndexPage
