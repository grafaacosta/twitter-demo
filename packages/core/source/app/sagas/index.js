import { all, fork } from 'redux-saga/effects'
import tweetsFetchers from '@/tweets/sagas'

const rootSaga = function*() {
    yield all([
        fork(tweetsFetchers)
    ])
}

export default rootSaga
