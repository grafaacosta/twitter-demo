import { put, call } from 'redux-saga/effects'

import { configResponse } from '@/app/reducer/app'

export const fetchConfig = function*() {
    try {
        const res = yield call(fetch, '/api/config')
        const data = yield call([res, res.json])
        console.log('config: ', data)
        yield put(configResponse({ config: data.data.config }))
    } catch (e) {
        yield put(configResponse({ error: e.message }))
    }
}
