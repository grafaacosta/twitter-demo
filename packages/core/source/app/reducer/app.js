import { createDuck } from 'redux-duck'

const ns = createDuck('app')

export const CONFIG_RESPONSE = ns.defineType('CONFIG_RESPONSE')
export const configResponse = ns.createAction(CONFIG_RESPONSE)

export const REQ_CONFIG = ns.defineType('REQ_CONFIG')
export const reqConfig = ns.createAction(REQ_CONFIG)

export const iniState = {
    config: {
        apiUrl: '/base',
    },
    initialized: false,
    fetching: false,
    error: {},
}

export default ns.createReducer(
    {
        [CONFIG_RESPONSE]: (state, { payload: { error, config } }) =>
            error
                ? {
                      ...state,
                      fetching: false,
                      error: {
                          ...state.error,
                          all: error,
                      },
                  }
                : {
                      ...state,
                      config,
                      fetching: false,
                      initialized: true,
                      error: {},
                  },
        [REQ_CONFIG]: state => ({
            ...state,
            fetching: true,
        }),
    },
    iniState,
)
