import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import tweets from '@/tweets/reducer'
import app from '@/app/reducer/app'

export default combineReducers({
    routing: routerReducer,
    app,
    tweets,
})
