
export const getApiUrl = state =>
    state.app.config.apiUrl

export const getInitialized = state =>
    state.app.config.initialized

export const getPathname = state =>
    state.routing.locationBeforeTransitions.pathname
