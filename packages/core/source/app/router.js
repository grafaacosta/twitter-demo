import React from 'react'

import { Router, Route, IndexRedirect } from 'react-router'

import Index from '@/pages/index'
import SearchPage from '@/pages/search'
import TweetDetails from '@/tweets/view/details'

export default history => () => (
    <Router history={history}>
        <Route path={'/'} component={Index}>
            <IndexRedirect to={'/tweets'} />
            <Route path={'/tweets'} component={SearchPage} />
            <Route path={'/tweets/:tweetId'} component={TweetDetails} />
        </Route>
    </Router>
)
