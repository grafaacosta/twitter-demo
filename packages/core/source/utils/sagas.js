import { delay } from 'redux-saga'
import { take, fork, cancel, call } from 'redux-saga/effects'

const debounceDelay = function*(delegate, ms) {
    yield call(delay, ms)
    yield call(delegate)
}

export const debounce = ({ action, delegate, ms }) => {
    return function*() {
        let task
        while (true) {
            yield take(action)
            if (task) {
                yield cancel(task)
            }
            task = yield fork(debounceDelay, delegate, ms)
        }
    }
}
