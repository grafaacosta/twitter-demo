import { put, take, select } from 'redux-saga/effects'
import sagaHelper from 'redux-saga-testing'

import { LOCATION_CHANGE } from 'react-router-redux'
import { watchTweetRoute } from './sagas'

import { reqTweets } from '@/tweets/reducer'
import { getTweetById } from '@/tweets/selectors'
import { EMPTY_TWEET } from '@/tweets/constants'

describe('watchTweetDetails saga', () => {
    const it = sagaHelper(watchTweetRoute())

    const tweetId = '01001129'

    global.fetch = () => {}

    it('Should wait for a location change action', result => {
        expect(result).toEqual(take(LOCATION_CHANGE))

        return { payload: { pathname: '/tweets/' + tweetId } }
    })

    it('Should try to find the tweet from the state', result => {
        expect(result).toEqual(select(getTweetById, tweetId))

        return EMPTY_TWEET
    })

    it('Should dispatch reqTweets with id', result => {
        expect(result).toEqual(put(reqTweets({ id: tweetId })))
    })
})
