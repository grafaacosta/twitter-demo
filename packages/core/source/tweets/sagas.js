import {
    select,
    take,
    put,
    call,
    takeEvery,
} from 'redux-saga/effects'

import { debounce } from '@/utils/sagas'

import { LOCATION_CHANGE } from 'react-router-redux'

import {
    SET_SEARCH_TEXT,
    REQ_TWEETS,
    reqTweets,
    tweetsResponse,
} from '@/tweets/reducer'
import { getSearchText, getTweetById } from '@/tweets/selectors'
import { EMPTY_TWEET } from '@/tweets/constants'

import { getApiUrl } from '@/app/selectors'

const fetchTweets = function*(action) {
    const baseUrl = yield select(getApiUrl)

    let url

    if (action) {
        const { payload: { id } } = action
        url = `${baseUrl}/tweets/${id}`
    } else {
        const text = yield select(getSearchText)
        if (text === '') {
            return
        }

        url = encodeURI(`${baseUrl}/tweets?text=${text}`)
    }

    try {
        const res = yield call(fetch, url)
        const data = yield call([res, res.json])
        yield put(tweetsResponse({ tweets: data.tweets }))
    } catch (e) {
        yield put(tweetsResponse({ error: e.message }))
    }
}

// fetch watchers

const watchFetchTweets = function*() {
    yield takeEvery(REQ_TWEETS, fetchTweets)
}

// user action watchers

export const watchSearchTweets = debounce({
    action: SET_SEARCH_TEXT,
    delegate: fetchTweets,
    ms: 400,
})

export const watchTweetRoute = function*() {
    for (;;) {
        const action = yield take(LOCATION_CHANGE)
        const re = /\/tweets\/(\d+)/
        if (re.test(action.payload.pathname)) {
            const id = re.exec(action.payload.pathname)[1]

            const tweet = yield select(getTweetById, id)

            if (tweet === EMPTY_TWEET) {
                yield put(reqTweets({ id }))
            }
        }
    }
}

export default watchFetchTweets
