export const EMPTY_TWEET = {
    text: '',
    id: '@@emptyTweet',
    profileImgUrl: '',
    profileBackgroundImageUrl: '',
    profileBackgroundColor: '',
    userName: '',
}
