import reducer, {
    iniState,
    tweetsResponse,
    setSearchText,
} from './reducer'

describe('setSearchTweet action', () => {
    test('Should set the right text to search', () => {
        const before = {
            ...iniState,
            fetching: 'all',
            error: {
                ...iniState.error,
                all: '',
            },
        }

        const expected = {
            ...iniState,
            searchText: 'node',
            fetching: 'all',
            error: {
                ...iniState.error,
                all: '',
            },
        }

        const actual = reducer(before, setSearchText({ text: 'node' }))

        expect(actual).toEqual(expected)
    })

    test('Should set the search response', () => {
        const before = {
            ...iniState,
            fetching: 'all',
        }

        const expected = {
            ...iniState,
            fetching: '',
            byId: {
                '01': {
                    id: '01',
                },
                '02': {
                    id: '02',
                },
            },
            error: {
                ...iniState.error,
                all: '',
            },
        }

        const tweets = [{ id: '01' }, { id: '02' }]

        const actual = reducer(before, tweetsResponse({ tweets }))

        expect(actual).toEqual(expected)
    })

    test('Should set the search response error', () => {
        const before = {
            ...iniState,
            fetching: 'all',
        }

        const expected = {
            ...iniState,
            fetching: '',
            byId: {},
            error: {
                all: 'Oops',
            },
        }

        const actual = reducer(before, tweetsResponse({ error: 'Oops' }))

        expect(actual).toEqual(expected)
    })
})
