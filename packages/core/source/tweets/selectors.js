import { createSelector } from 'reselect'
import { EMPTY_TWEET } from '@/tweets/constants'

export const getSearchText = state => state.tweets.searchText

export const getAllTweets = createSelector(
    state => state.tweets.byId,
    byId => Object.values(byId),
)

export const getTweetById = (state, id) =>
    state.tweets.byId.hasOwnProperty(id) ? state.tweets.byId[id] : EMPTY_TWEET
// mappers

export const mapFromTweeter = tweet => ({
    text: tweet.text,
    id: tweet.id_str,
    profileImgUrl: tweet.user.profile_image_url,
    profileBackgroundImageUrl: tweet.user.profile_banner_url,
    profileBackgroundColor: tweet.user.profile_background_color,
    userName: tweet.user.screen_name,
})
