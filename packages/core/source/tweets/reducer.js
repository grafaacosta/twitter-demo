import { createDuck } from 'redux-duck'
import { indexBy, prop } from 'ramda'

const ns = createDuck('tweets')

export const SET_SEARCH_TEXT = ns.defineType('SET_SEARCH_TEXT')
export const setSearchText = ns.createAction(SET_SEARCH_TEXT)

export const REQ_TWEETS = ns.defineType('REQ_TWEETS')
export const reqTweets = ns.createAction(REQ_TWEETS)

export const TWEETS_RESPONSE = ns.defineType('TWEETS_RESPONSE')
export const tweetsResponse = ns.createAction(TWEETS_RESPONSE)

export const iniState = {
    searchText: '',
    fetching: '',
    error: {
        all: '',
    },
    byId: {},
}

const byId = indexBy(prop('id'))

export default ns.createReducer(
    {
        [SET_SEARCH_TEXT]: (state, { payload: { text } }) => ({
            ...state,
            searchText: text,
        }),
        [REQ_TWEETS]: state => ({
            ...state,
            fetching: 'all'
        }),
        [TWEETS_RESPONSE]: (state, { payload: { error, tweets } }) =>
            error
                ? {
                      ...state,
                      fetching: '',
                      error: {
                          ...state.error,
                          all: error,
                      },
                  }
                : {
                      ...state,
                      byId: byId(tweets),
                      fetching: '',
                  },
    },
    iniState,
)
