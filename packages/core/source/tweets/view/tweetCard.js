import React from 'react'
import styled from 'styled-components'

import Paper from 'material-ui/Paper'
import Button from 'material-ui/Button'

import { Link } from 'react-router'

import {
    ProfilePicContainer,
    DescriptionContainer,
    UserName,
    TweetText,
    CardActions,
} from '@/ui-kit/tweets'

const TweetCardContainer = styled(Paper)`
    display: flex;
    flex: 1;
    min-height: 150px;
    min-width: 450px;
    margin: 5px;
    align-items: stretch;
`

const TweetCard = ({ tweet }) => (
    <TweetCardContainer className="tweetCard">
        <ProfilePicContainer>
            <img src={tweet.profileImgUrl} />
        </ProfilePicContainer>
        <DescriptionContainer>
            <UserName>@{tweet.userName}</UserName>
            <TweetText component="h2">{tweet.text}</TweetText>
            <CardActions>
                <Button size="small" color="primary" className="cardDetails">
                    <Link
                        to={'/tweets/' + tweet.id}
                        style={{ textDecoration: 'none' }}
                    >
                        Details
                    </Link>
                </Button>
            </CardActions>
        </DescriptionContainer>
    </TweetCardContainer>
)

export default TweetCard
