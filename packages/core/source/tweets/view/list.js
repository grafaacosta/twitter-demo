import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

import { getAllTweets } from '@/tweets/selectors'

import TweetCard from '@/tweets/view/tweetCard'

const TweetsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`

const TweetList = ({ tweets }) => (
    <TweetsContainer>
        {tweets.map(tweet => <TweetCard key={tweet.id} tweet={tweet} />)}
    </TweetsContainer>
)

export default connect(state => ({
    tweets: getAllTweets(state),
}))(TweetList)
