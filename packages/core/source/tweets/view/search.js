import React from 'react'
import { connect } from 'react-redux'

import { setSearchText } from '@/tweets/reducer'
import { getSearchText } from '@/tweets/selectors'

import TextField from 'material-ui/TextField'

const SearchTweet = ({ value, onChange }) => (
    <TextField
        label="Search Tweet"
        InputLabelProps={{
            shrink: true,
        }}
        fullWidth
        margin="normal"
        value={value}
        onChange={onChange}
    />
)

export default connect(
    state => ({
        value: getSearchText(state),
    }),
    dispatch => ({
        onChange: e => dispatch(setSearchText({ text: e.target.value })),
    }),
)(SearchTweet)
