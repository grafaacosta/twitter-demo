import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Paper from 'material-ui/Paper'

import { getTweetById } from '@/tweets/selectors'

import {
    ProfilePicContainer,
    DescriptionContainer,
    UserName,
    TweetText,
} from '@/ui-kit/tweets'

const TweetDescriptionContainer = styled(Paper)`
    display: flex;
    flex-direction: column;
    flex: 1;
    margin: 5px;
    align-items: stretch;
    width: 700px;
    align-self: center;
`

const BackgroundPicContainer = styled.div`
    height: 200px;
    overflow: hidden;
    position: relative;
    background-image: url(${props => props.imageUrl});
    background-color: ${props => props.color};
    background-size: cover;
`

const ProfilePicAndUser = styled.div`
    position: absolute;
    z-index: 5;
    bottom: 10px;
    left: 10px;
    display: flex;
`

const TweetDetails = ({ tweet }) => (
    <TweetDescriptionContainer className="details">
        <BackgroundPicContainer
            imageUrl={tweet.profileBackgroundImageUrl}
            color={'#' + tweet.profileBackgroundColor}
        >
            <ProfilePicAndUser>
                <ProfilePicContainer>
                    <img src={tweet.profileImgUrl} />
                </ProfilePicContainer>
                <UserName large>@{tweet.userName}</UserName>
            </ProfilePicAndUser>
        </BackgroundPicContainer>
        <DescriptionContainer>
            <TweetText component="h2">{tweet.text}</TweetText>
        </DescriptionContainer>
    </TweetDescriptionContainer>
)

export default connect((state, ownProps) => ({
    tweet: getTweetById(state, ownProps.params.tweetId),
}))(TweetDetails)
