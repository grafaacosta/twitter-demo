import 'isomorphic-fetch'

import config from 'config'
import twitter from 'twitter'

import makeApp from '@/app'
import { fetchTwitterBearer } from '@/auth/service'

const consumer_key = config.get('twitter.auth.key')
const consumer_secret = config.get('twitter.auth.secret')

const host = config.get('api.host')
const port = config.get('api.port')

fetchTwitterBearer({ key: consumer_key, secret: consumer_secret }).then(
    data => {
        const client = new twitter({
            consumer_key,
            consumer_secret,
            bearer_token: data.access_token,
        })

        const app = makeApp({ twitterClient: client })

        app.listen(port, host, err => {
            if (err) {
                console.error(err)
                return
            }

            console.log(`Listening at http://${host}:${port}`)
        })
    },
)
