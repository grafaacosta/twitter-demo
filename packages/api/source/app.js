import express from 'express'
import apicache from 'apicache'

import { getTweets, getTweet } from '@/tweets/routes'

const cache = apicache.middleware

const makeApp = ({ app = express(), twitterClient }) => {
    app.get('/tweets', cache('1minute'), getTweets({ twitterClient }))
    app.get('/tweets/:id', getTweet({ twitterClient }))

    return app
}

export default makeApp
