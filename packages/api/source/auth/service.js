import config from 'config'

const tUrl = config.get('twitter.auth.barerUrl')
const tKey = config.get('twitter.auth.key')
const tSecret = config.get('twitter.auth.secret')

export const fetchTwitterBearer = ({
    authUrl = tUrl,
    key = tKey,
    secret = tSecret,
} = {}) => {
    const cat = `${key}:${secret}`

    const credentials = new Buffer(cat).toString('base64')

    const barerRequest = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: 'Basic ' + credentials,
        },
        body: 'grant_type=client_credentials',
    }

    return fetch(authUrl, barerRequest).then(req => req.json())
}
