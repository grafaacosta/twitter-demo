import { mapFromTweeter } from 'core/lib/tweets/selectors'
import { indexBy, prop, merge } from 'ramda'

let tweetsById = {} // tweet cache
const byId = indexBy(prop('id'))

export const getTweets = ({ twitterClient }) => (req, res) => {
    twitterClient.get('search/tweets', { q: req.query.text }, (error, data) => {
        const tweets =
            (data.statuses && data.statuses.map(mapFromTweeter)) || []

        tweetsById = merge(tweetsById, byId(tweets))

        res.json({
            tweets,
        })
    })
}

export const getTweet = () => (req, res) => {
    if (tweetsById.hasOwnProperty(req.params.id)) {
        res.json({
            tweets: [tweetsById[req.params.id]],
        })
    } else {
        res.status(400).send('Tweet not found')
    }
}
