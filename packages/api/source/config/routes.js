import config from 'config'

export const getClientConfig = () => (req, res) =>
    res.json({
        data: {
            config: config.get('client'),
        },
    })

export const getStaticServerConfig = () => (req, res) =>
    res.json({
        data: {
            config: config.get('staticServer'),
        },
    })
