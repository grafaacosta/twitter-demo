import { all, fork } from 'redux-saga/effects'

import fetchWatchers from 'core/lib/app/sagas'
import tweetsSagas from '@/tweets/sagas'

const rootSaga = function*() {
    yield all([
        fork(fetchWatchers),
        fork(tweetsSagas),
    ])
}

export default rootSaga
