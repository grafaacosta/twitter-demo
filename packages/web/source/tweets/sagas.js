import { all, fork } from 'redux-saga/effects'

import { watchSearchTweets, watchTweetRoute } from 'core/lib/tweets/sagas'

const rootSaga = function*() {
    yield all([
        fork(watchSearchTweets),
        fork(watchTweetRoute)
    ])
}

export default rootSaga
