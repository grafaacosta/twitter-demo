## Requeriments

* Node
* Yarn
* Java for e2e tests with selenium

## Getting Started

In the root directory of the repo

```
yarn install
yarn run all
```

This script will install deps, run eslint, unit tests with jest, start the server and api with pm2 and run functional tests with nightwatch

Now the app should be running at http://localhost:8080/

Also you can run:

```
yarn run clean
yarn run lint
yarn run test
yarn run build
yarn run start
yarn run stop
yarn run test:e2e
```

## How it works

* The main page is /tweets where you can search by text
* When you search by text a tweet list should appear and you can click in the details link to view further details

## Missing Features

* Add more details to the tweet details page
* Add navigation between search and details
* Unit test components with enzyme
* Add loaders when fetching data

* Update to react 16 react router v4 and figure out how to sync router v4 with the state

## Tech stack

* Lerna
* Babel
* Redux
* Redux sagas
* React
* Styled Components
* React router
* Express
* Node config
* Jest
* Nightwatch
* Pm2


## What's inside?

The repo has a monorepo structure with lerna and yarn workspaces. The idea is to put reusable code into the core package and be able to churn out
applications like web, static-server, api, maybe a native app in the future.

* `packages/core`    - Shared code.
* `packages`/web` - For browser-only code.
* `packages/static-server` - For static server code.
* `packages/api` - For api code.
* `packages/e2e` - For e2e tests code.

## PD

As always I enjoyed doing this assignments. I hope you like it, I did it with love.
