var path = require('path')

const homeFolder = path.join(__dirname, '../')

module.exports = {
    name: 'Twitter Demo',
    folders: {
        home: homeFolder,
        client: {
            static: path.join(homeFolder, 'packages', 'web', 'build'),
        },
    },
    log: {
        params: false,
    },
    static: {
        host: '0.0.0.0',
        port: '8080',
    },
    api: {
        host: '0.0.0.0',
        port: '8081',
    },
    client: {
        apiUrl: '/api'
    },
    staticServer: {
        apiUrl: 'http://localhost:8081'
    },
    test: {
        folders: {
            reports:
                process.env.CIRCLE_TEST_REPORTS ||
                path.join(homeFolder, 'packages', 'e2e', 'reports'),
        },
    },
    twitter: {
        auth: {
            barerUrl: 'https://api.twitter.com/oauth2/token',
            key: 'f8BEXLisgQNLmBrYaeDW7Qgit',
            secret: 'WoJ7AGigRAAZhXTTElIhq7KvfHwaZfPN5mGTJXGiyftZFWnFEX',
        },
    },
}
